var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var socketInfo = {};

var lastEmits = [];

const addMessage = (msg) => {
  if (lastEmits.length > 10) {
    lastEmits.shift();
  }

  lastEmits.push(msg);
};

const emitMessage = (name, msg) => {
  const patchedMsg = {
    ...msg,
    date: new Date()
  };
  console.log({ name, msg: patchedMsg });
  io.emit(name, patchedMsg);
  addMessage({ name, msg: patchedMsg });
};

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html', { "Cache-Control": "no-cache", });
});

io.on('connection', function(socket){
  console.log('a user connected');

  if (lastEmits.length > 0) {
    console.log("Rewind!", `Trying to resend ${lastEmits.length} emits to ${socket.id}`);
    // send last messages to user
    for (const emit in lastEmits) {
      socket.emit(lastEmits[emit].name, lastEmits[emit].msg);
    }
  }

  socket.on('chat-message', function(msg){
    console.log(`message: ${msg.nick}: ${msg.text}`);
    emitMessage('chat-message', msg);
  });

  socket.on('disconnect', function(){
    if (socketInfo[socket.id] && socketInfo[socket.id]['nick']) {
      const nick = socketInfo[socket.id]['nick'];
      emitMessage('user-leave', { nick });

      console.log(`user ${nick} disconnected`);
    } else {
      console.log('user-disconnected');
    }
  });

  socket.on('nick', function(msg){
    console.log(`User ${msg.nick} has connected.`);
    emitMessage('nick', msg);

    if (!socketInfo[socket.id]) {
      socketInfo[socket.id] = {};
    }

    socketInfo[socket.id]['nick'] = msg.nick;
  });
});

http.listen(4000, function(){
  console.log('listening on *:4000');
});